$(function(){
    $('.paypal-cart').submit(function(){
        
        var succeed = false;
        var block = $(this);
        $.ajax({
            url:Routing.generate('commerce_paypal_paypalcart'),
            async:false,
            success: function(data)
            {
                if(data['login'])
                {
                   succeed = true;
                        
                        $("[name=discount_amount_cart]", block).attr("value",data["discountAmount"]);
                    
                }else
                {
                    alert("Erreur");
                }
            },
            error: function()
            {
            },
         });
         return succeed;
    });
});

