
$(function(){
    $('#ajax-form').submit(function(){

        $.ajax({
            url:Routing.generate('twc_driver_contact'),
            data:$(this).serialize(),
            type: "POST",
            async: false,
            success:function(data)
            {
                if(data != "good")
                {
                    $("#contact-message").text('');

                    $.each(data, function(index,value){

                        $("#contact-message").append(value+'<br>');
                    });
                }else
                {
                    $("#contact-message").text('Votre message a bien été envoyé');
                    $("#ajax-form").each (function(){
                         this.reset();
                       });

                }
                $('html,body').animate({scrollTop: $("#contact").offset().top}, 'slow');

            },
            error:function(data)
            {
                alert("error");
            }

        });

        return false;
    });

});
