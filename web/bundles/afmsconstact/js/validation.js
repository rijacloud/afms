$( ".ajax-contact-form" ).validate({
//    highlight: function(element){
//        $(element).parents(".bd_wrap").find(".check").removeClass("check_off check_on").addClass("check_cross");
//    },
//    unhighlight: function(element){
//        $(element).parents(".bd_wrap").find(".check").removeClass("check_off check_cross").addClass("check_on");
//    },
    rules: {
        "rdv_contactbundle_contact[name]": {
            required: true
        },
        "rdv_contactbundle_contact[firstname]": {
            required: true
        },
        "rdv_contactbundle_contact[tel]": {
            required: true,
            regex: /^(0)[0-9]{9}$/,
        },
        "rdv_contactbundle_contact[subject]": {
            required: true,
        },
        
        "rdv_contactbundle_contact[email]": {
            required: true,
            email: true
        },
        "rdv_contactbundle_contact[message]": {
            required: true,
        },
    }
});

jQuery.extend(jQuery.validator.messages, {
    required: "Champ obligatoire.",
    remote: "Valeur non conforme.",
    email: "Email incorrecte.",
    url: "Valeur non conforme.",
    date: "Valeur non conforme.",
    dateISO: "Valeur non conforme.",
    number: "Valeur non conforme.",
    digits: "Valeur non conforme.",
    creditcard: "Valeur non conforme.",
    equalTo: "Valeur non conforme.",
    accept: "Valeur non conforme.",
    maxlength: jQuery.validator.format("au maximum {0} caractères."),
    minlength: jQuery.validator.format("au minimum {0} caractères"),
    rangelength: jQuery.validator.format("Please enter a value between {0} and {1} characters long."),
    range: jQuery.validator.format("Please enter a value between {0} and {1}."),
    max: jQuery.validator.format("La valeur doit être inférieur à {0}."),
    min: jQuery.validator.format("La valeur doit être supérieur à {0}.")
});

$(function(){
    initRestrictSaisi();
})
/* 
*  Car numérique interdit
*/	

function initRestrictSaisi(){
	var regInputAlpha = /^[a-zA-Zäàçéèêëîïñôöùûü' \-]*$/ ;
	var regInputAlphaNumeric = /^[a-zA-Zäàçéèêëîïñôöùûü' 0-9]*$/ ;
	var regInputAlphaNumericNospace = /^[a-zA-Zäàçéèêëîïñôöùûü0-9]*$/ ;
	var regInputBonusCode = /^[a-zA-Z0-9]*$/ ;
	var regInputNumeric = /^[0-9]*$/ ;
	var regInputDate = /^[0-9\/]*$/ ; 
	var regInputDepartement = /^[0-9ABab]*$/ ;
	var regInputEmail = /^([^\x00-\x20\x22\x28\x29\x2c\x2e\x3a-\x3c\x3e\x40\x5b-\x5d\x7f-\xff]+|\x22([^\x0d\x22\x5c\x80-\xff]|\x5c[\x00-\x7f])*\x22)(\x2e([^\x00-\x20\x22\x28\x29\x2c\x2e\x3a-\x3c\x3e\x40\x5b-\x5d\x7f-\xff]+|\x22([^\x0d\x22\x5c\x80-\xff]|\x5c[\x00-\x7f])*\x22))*\x40([^\x00-\x20\x22\x28\x29\x2c\x2e\x3a-\x3c\x3e\x40\x5b-\x5d\x7f-\xff]+|\x5b([^\x0d\x5b-\x5d\x80-\xff]|\x5c[\x00-\x7f])*\x5d)(\x2e([^\x00-\x20\x22\x28\x29\x2c\x2e\x3a-\x3c\x3e\x40\x5b-\x5d\x7f-\xff]+|\x5b([^\x0d\x5b-\x5d\x80-\xff]|\x5c[\x00-\x7f])*\x5d))*$/ ; 
	// /^[a-zA-Z0-9_\-.@]*$/ ;
	var regInputPhone = /^[0-9\+]*$/ ;
	var regInputTwitter = /^[a-zA-Zäàçéèêëîïñôöùûü' 0-9_]*$/ ;
	var reg_input = null ;
	
	$(".restrict-alpha-numeric").bind("keypress", function(event) {
		// reg_input = regInputAlphaNumeric ;
		applyRestrict(event, regInputAlphaNumeric);
	});
	$(".restrict-alpha-numeric-nospace").bind("keypress", function(event) {
		reg_input = regInputAlphaNumericNospace ;
		applyRestrict(event, regInputAlphaNumericNospace);
	});
	$(".restrict-alpha").bind("keypress", function(event) {
		reg_input = regInputAlpha ;
		applyRestrict(event, regInputAlpha);
	});
	$(".restrict-numeric").bind("keypress", function(event) {
		reg_input = regInputNumeric ;
		applyRestrict(event, regInputNumeric);
	});
	$(".restrict-date").bind("keypress", function(event) {
		reg_input = regInputDate ;
		applyRestrict(event, regInputDate);
	});
	$(".restrict-departement").bind("keypress", function(event) {
		reg_input = regInputDepartement ;
		applyRestrict(event, regInputDepartement);
	});
	$(".restrict-email").bind("keypress", function(event) {
		reg_input = regInputEmail ;
		// applyRestrict(event, regInputEmail);
	});
	$(".restrict-phone").bind("keypress", function(event) {
            console.log("ici");
		reg_input = regInputPhone ;
		applyRestrict(event, regInputPhone);
	});
	$(".restrict-twitter").bind("keypress", function(event) {
		reg_input = regInputTwitter ;
		applyRestrict(event, regInputTwitter);
	});
	$(".restrict-bonuscode").bind("keypress", function(event) {
		reg_input = regInputBonusCode ;
		applyRestrict(event, regInputBonusCode);
	});

	if( reg_input == null )return;
		
	function applyRestrict(event, reg_input)
	{
		var evt=event||window.event;
	    
		var kc = evt.keyCode; // Sur FF, keycode vaut 0 si on appuie sur une touche d'action (suppr)
		var kw = 0;
//		if ($.browser.mozilla) {
//			kw = event.which;
//		}
kw = event.which;
		var c = ( kw == 0 ) ? String.fromCharCode(kc) : String.fromCharCode(kw);
		
		if (/Opera/.test(navigator.userAgent)) {
			// Si opera
		}
		else {
			if (!(reg_input.test(c))) {
				if (kw != kc) {
//					if ($.browser.mozilla && kc == 0) {
//						event.preventDefault();
//						event.stopPropagation();
//					}
//					else if (!$.browser.mozilla) {
//						event.preventDefault();
//						event.stopPropagation();
//					}
                                                event.preventDefault();
						event.stopPropagation();
				}
			}
		}
	}
}

//$('input[name="rdv_contactbundle_contact[tel]"]').rules("add",{
//            minlength: 10,
//            regex: /^(06|07)[0-9]{8}$/,
//            messages : {
//                    minlength: "Champ obligatoire",
//                    regex: "Doit commencer par 06 ou 07 suivi de 8 chiffres"
//            }
//    });
    
/* Validation */
	/*
	 * http://randomactsofcoding.blogspot.fr/2008/10/starting-with-jquery-how-to-write.html
	 */
//	$.extend($.validator.messages, {
//		// redefinir les messages d'erreur
//		required: "Champ obligatoire",
//		email: "Merci de renseigner un mail valide",
//		number:"Merci de renseigner des numéros",
//		maxlength: jQuery.validator.format("Au maximum {0} caractères."),
//		minlength: jQuery.validator.format("Au minimum {0} caractères."),
//		range: jQuery.validator.format("Date comprise entre {0} et {1}."),
//		min: jQuery.validator.format("Montant supérieur à {0}.")
//	});	
	$.validator.addMethod(
		"email",
		function(value, element) {
			var reg = new RegExp("^[A-Z0-9][A-Z0-9._-]*@[A-Z0-9.-]+[\.]{1}[A-Z]{2,6}$");
		      return reg.test(value.toUpperCase());
		   }, "Merci de renseigner un mail valide"
	);	

	$.validator.addMethod(
	  "regex",
	   function(value, element, regexp) {
	       if (regexp.constructor != RegExp)
	          regexp = new RegExp(regexp);
	       else if (regexp.global)
	          regexp.lastIndex = 0;
	          return this.optional(element) || regexp.test(value);
	   }, "Veuillez saisir un numéro valide"
	);	
	$.validator.addMethod(
        "regexCustom",
        function(value, element, regexp) {
            if (regexp.constructor != RegExp)
                regexp = new RegExp(regexp);
            else if (regexp.global)
                regexp.lastIndex = 0;
                return this.optional(element) || regexp.test(value);
        }, 
        "Valeur non conforme"
	);
	jQuery.validator.addMethod(
		"select",
		function(value, element) {
			if( value.length == 0 || value == "0" ){
				return this.optional(element) || false ;
			} else {
				return true;
			}		
		}, "Champ obligatoire"
	);