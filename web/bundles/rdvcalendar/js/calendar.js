
	$(document).ready(function() {
		$('#calendar').fullCalendar({
		 dayClick: function(date, jsEvent, view) {
		 var date = $.fullCalendar.moment(date);
		 $('#calendar').fullCalendar('changeView', 'agendaDay');
		 $('#calendar').fullCalendar( 'gotoDate',date);
			
        },
			header: {
				left: 'prev,next today',
				center: 'title',
				right: 'month,agendaWeek,agendaDay'
			},
			//defaultDate: '2014-09-12',
			selectable: false,
			selectHelper: true,
			
			editable: true,
			eventLimit: true, // allow "more" link when too many events
                        events: Routing.generate('rdv_calendar_getcalendar'),
//			events: [
//				{
//					title: 'All Day Event',
//					start: '2014-09-01',
//					editable: true
//				},
//				
//			],
    
		});
		//$('#calendar').fullCalendar('changeView', 'agendaWeek');
	});
