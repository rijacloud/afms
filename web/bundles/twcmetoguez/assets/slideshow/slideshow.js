(function($) {

    "use strict";


    var Slideshow, Animations;

    if (!$.fn.velocity) {
        $.fn.velocity = $.fn.animate;
    }

    Slideshow = function(element, options) {

        this.options = $.extend({}, Slideshow.defaults, options);

        var $this            = this,
            $element         = $(element);

        if($element.data("slideshow")) return;

        this.element     = $(element);
        this.container   = this.element.find('.slideshow');
        this.slides      = this.container.children();
        this.current     = this.options.start;
        this.animating   = false;
        this.triggers    = this.element.find('[data-goto]');

        // Set animation effect
        this.element.addClass(this.options.animation);

        // set background image from img

        var canvas, placeholder, sizer, img;

        this.slides.each(function() {

            var slide = $(this), media = slide.children('img,video,iframe').eq(0);

            slide.data('media', media);

            if (media[0]) {

                media       = media.eq(0);
                placeholder = false;

                switch(media[0].nodeName) {
                    case 'IMG':
                        slide.css({"background-image":"url("+ media.attr("src") + ")"});
                        sizer = media;
                        break;
                    case 'IFRAME':

                        var api = 'enablejsapi=1&api=1', src = media[0].src;

                        media.attr('src', [src, (media[0].src.indexOf('?') > -1 ? '&':'?'), api].join(''));

                        placeholder = true;

                        break;
                    case 'VIDEO':
                        placeholder = true;
                }

                if (placeholder) {

                    canvas = $('<canvas></canvas>').attr({'width':media[0].width, 'height':media[0].height});
                    img    = $('<img style="width:100%;height:auto;">').attr('src', canvas[0].toDataURL());

                    slide.prepend(img);
                    sizer = img;
                }

                slide.data('sizer', sizer.addClass('slideshow-sizer'));
            }

        });

        this.element.on("click", '[data-goto]', function(e) {

            e.preventDefault();

            var slide = $(this).data('goto');

            if($this.current == slide) return;

            switch(slide) {
                case 'next':
                case 'previous':
                    $this[slide=='next' ? 'next':'previous']();
                    break;
                default:
                    $this.show(slide);
            }

        });

        // Set start slide
        this.slides.eq(this.current).addClass("active");
        this.element.find('[data-goto]').removeClass("active").filter('[data-goto="'+this.current+'"]').addClass("active");

        // handle resize
        $(window).on("resize load", debounce(function(){ $this.resize(); }, 100));
        this.resize();

        // Set autoplay
        if(this.options.autoplay) {
            this.start();
        }

        this.element.on({
            'mouseenter': function() { $this.hovering = true;  },
            'mouseleave': function() { $this.hovering = false; }
        });

        this.element.on("swipeRight swipeLeft", function(e) {
            $this[e.type=='swipeLeft' ? 'next':'previous']();
        });

        this.element.data("slideshow", this);
    };

    // Class definition
    Slideshow.prototype = {

        current  : false,
        interval : null,
        hovering : false,

        resize: function() {

            if (this.element.hasClass('slideshow-fullscreen')) return;

            var $this = this, height = 0, sizer;


            this.container.css("height", "").children().css("height", "").each(function() {
                sizer  = $(this).data('sizer') || $(this);
                height = Math.max(height, sizer.height());
            });

            if (this.options.maxheight && height > this.options.maxheight) {
                height = this.options.maxheight;
            }

            this.container.css("height", height).children().css("height", height);
        },

        show: function(index, direction) {

            if (this.animating) return;

            this.animating = true;

            var $this        = this,
                current      = this.slides.eq(this.current),
                next         = this.slides.eq(index),
                dir          = direction ? direction : this.current < index ? -1 : 1,
                currentmedia = current.data('media'),
                animation    = Animations[this.options.animation] ? this.options.animation : 'fade',
                nextmedia    = next.data('media'),
                finalize     = function() {

                    if(!$this.animating) return;

                    if(currentmedia.is('video,iframe')) {
                        $this.pausemedia(currentmedia);
                    }

                    if(nextmedia.is('video,iframe')) {
                        $this.playmedia(nextmedia);
                    }

                    next.addClass("active");
                    current.removeClass('active');

                    $this.animating = false;
                    $this.current   = index;
                };

            animation = 'slide';

            Animations[animation].apply(this, [current, next, dir]).then(finalize);

            $this.triggers.filter('[data-goto="'+$this.current+'"]').removeClass('active').end().filter('[data-goto="'+index+'"]').addClass('active');

        },

        next: function() {
            this.show(this.slides[this.current + 1] ? (this.current + 1) : 0);
        },

        previous: function() {
            this.show(this.slides[this.current - 1] ? (this.current - 1) : (this.slides.length - 1));
        },

        start: function() {

            this.stop();

            var $this = this;

            this.interval = setInterval(function() {
                if(!$this.hovering) $this.show($this.options.start, $this.next());
            }, this.options.duration);
        },

        stop: function() {
            if(this.interval) clearInterval(this.interval);
        },

        playmedia: function(media) {

            switch(media[0].nodeName) {
                case 'VIDEO':
                    media[0].play();
                    break;
                case 'IFRAME':
                    media[0].contentWindow.postMessage('{ "event": "command", "func": "playVideo", "method":"play"}', '*');
                    break;
            }

        },

        pausemedia: function(media) {

            switch(media[0].nodeName) {
                case 'VIDEO':
                    media[0].pause();
                    break;
                case 'IFRAME':
                    media[0].contentWindow.postMessage('{ "event": "command", "func": "pauseVideo", "method":"pause"}', '*');
                    break;
            }
        }
    };


    Animations = {

        'none': function() {

            var d = $.Deferred();

            d.resolve();

            return d.promise();
        },

        'fade': function(current, next) {

            var d = $.Deferred();

            current.css('z-index', 2).fadeOut();

            next.css('z-index', 1).fadeIn(this.options.duration, function() {
                d.resolve();
            });

            return d.promise();
        },

        'crossover': function(current, next, dir) {

            var d = $.Deferred();

            current.css({top:0, left: 0, opacity: 1}).velocity({left: (current.width() * dir), opacity: 0}, this.options.duration);

            next.css({top:0, opacity: 0, left: (current.width() * dir)}).velocity({left:0, opacity:1}, this.options.duration, function() {
                d.resolve();
            });

            return d.promise();
        },

        'slide': function(current, next, dir) {

            var d = $.Deferred();

            current.css({top:0, left: 0});
            next.css({top:0, left: (current.width() * dir * -1)}).addClass('active');

            current.velocity({left: (current.width() * dir)}, this.options.duration);

            next.velocity({left:0}, this.options.duration, function() {
                d.resolve();
            });

            return d.promise();
        }
    };

    Slideshow.animations = Animations;

    Slideshow.defaults = {
        animation     : "slide",
        duration      : 200,
        maxheight     : false,
        start         : 0,
        autoplay      : false
    };

    // init code
    function init() {
        $("[data-slideshow-init]").each(function() {

            var slideshow = $(this);

            if (!slideshow.data("slideshow")) {
                var obj = new Slideshow(slideshow, parseoptions(slideshow.attr('data-slideshow-init')));
            }
        });
    }

    // helper

    function parseoptions(string) {

        if ($.isPlainObject(string)) return string;

        var start = (string ? string.indexOf("{") : -1), options = {};

        if (start != -1) {
            try {
                options = (new Function("", "var json = " + string.substr(start) + "; return JSON.parse(JSON.stringify(json));"))();
            } catch (e) {}
        }

        return options;
    };

    function debounce(func, wait, immediate) {
        var timeout;
        return function() {
            var context = this, args = arguments;
            var later = function() {
                timeout = null;
                if (!immediate) func.apply(context, args);
            };
            var callNow = immediate && !timeout;
            clearTimeout(timeout);
            timeout = setTimeout(later, wait);
            if (callNow) func.apply(context, args);
        };
    };


    if(!window.MutationObserver) {

        try {

            var observer = new MutationObserver(debounce(function(mutations) {
                init();
            }, 50));

            // pass in the target node, as well as the observer options
            observer.observe(document.body, { childList: true, subtree: true });

        } catch(e) {}
    }

    $(init);

})(jQuery);
